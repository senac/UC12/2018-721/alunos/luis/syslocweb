/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Genero;

/**
 *
 * @author sala304b
 */
public class GeneroDAO extends DAO<Genero> {

    public GeneroDAO() {
        super(Genero.class);
    }

}
