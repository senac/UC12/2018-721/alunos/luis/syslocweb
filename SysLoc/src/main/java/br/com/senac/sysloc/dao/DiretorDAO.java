/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Diretor;

/**
 *
 * @author sala304b
 */
public class DiretorDAO extends DAO<Diretor> {

    public DiretorDAO() {
        super(Diretor.class);
    }
}
