/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sysloc.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Filme {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Column(length = 1000, nullable = false)
    private String Titulo;
    @Column(name = "ANO_LANCAOMENTO", nullable = false)
    private int Lancamento_ano;

    @Column(nullable = false)
    private int avaliacao;
    @Column(name = "TEMPO_DURACAO", nullable = false)
    private int duracao;
    @ManyToOne
    @JoinColumn(name = "ID_GENERO", nullable = false)
    private Genero genero;
    @ManyToOne
    @JoinColumn(name = "ID_DIRETOR", nullable = false)
    private Diretor diretor;
    @ManyToMany
    @JoinTable(name = "FILME_ATOR", joinColumns = @JoinColumn(name = "ID_FILME"), inverseJoinColumns = @JoinColumn(name = "ID_ATOR"))
    private List<Ator> atores;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    public int getLancamento_ano() {
        return Lancamento_ano;
    }

    public void setLancamento_ano(int Lancamento_ano) {
        this.Lancamento_ano = Lancamento_ano;
    }

    public int getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(int avaliacao) {
        this.avaliacao = avaliacao;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;

    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Diretor getDiretor() {
        return diretor;
    }

    public void setDiretor(Diretor diretor) {
        this.diretor = diretor;
    }

    public List<Ator> getAtores() {
        return atores;
    }

    public void setAtores(List<Ator> atores) {
        this.atores = atores;
    }

}
