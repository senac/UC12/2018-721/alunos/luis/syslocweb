/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.sysloc.dao;

import br.com.senac.sysloc.model.Filme;

/**
 * /**
 *
 * @author sala304b
 */
public class FilmeDAO extends DAO<Filme> {

    public FilmeDAO() {
        super(Filme.class);
    }

}
